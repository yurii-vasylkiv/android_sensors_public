package com.hci.sensoric;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.os.Process;

import com.hci.sensoric.network.AsyncClientCallback;
import com.hci.sensoric.network.AsyncTaskClientParams;
import com.hci.sensoric.network.ClientAsyncTask;
import com.hci.sensoric.network.ConnectionHandler;
import com.hci.sensoric.network.Message;
import com.hci.sensoric.network.Protocol;
import com.hci.sensoric.network.Server;
import com.hci.sensoric.sensors.BufferBasedSensorOrientationUpdateListener;
import com.hci.sensoric.sensors.SensorHandler;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity
{

	TextView readingYaw, readingPitch, readingRoll, statusTxt, stateTxt;
	Button startStopSensorBtn, exitBtn, startStopServerBtn;
	Map<String, ClientAsyncTask<BufferBasedSensorDataToBytesTranslator>> subscribers = new HashMap<>();
	private Vibrator m_vibrator;
	private SensorHandler interval_sensor_handler;
	private Protocol protocol = new Protocol();
	Server server;
	ConnectionHandler m_callback = new ConnectionHandler()
	{
		@Override
		public byte[] onReceiveRequest(byte[] request_data, String remote_host)
		{
			runOnUiThread(() -> stateTxt.setText("Received"));
			byte[] response;
			Message request_message = protocol.extract_message(request_data);
			switch (request_message.method)
			{
				case SingleGetRotationAngleEvent:
				{
					Log.d("Status:", "SingleGetRotationAngleEvent");
									/* Just a single request for a single response */
					response = protocol.
							generate_get_rotation_angle_response(interval_sensor_handler.get_XYZ_angle_values());
				}
				break;

				case SubscribeForRotationAngleEvents:
				{
					Log.d("Status:", "SubscribeForRotationAngleEvents");
					Log.d("Status: ", "registered threads: " + subscribers.size());

					/*
					 * Apparently, we receive a request for a subscription of this specific
					 * method. The next action would be to subscribe for a sensors update data.
					 * For that we have to:
					 * 1. start SensorHandler by calling start() - this will start listening
					 *    for sensors changes until we call stop().
					 * 2. Register the incoming client for those changes updates.
					 *
					 * Essentially, we are gonna send data all the time when buffer is not empty.
					 */
					BufferBasedSensorOrientationUpdateListener callback = new BufferBasedSensorDataToBytesTranslator();
					String task_id = request_message.subscribeForRotationAngleEventsMethod.task_id;
					AsyncTaskClientParams<BufferBasedSensorDataToBytesTranslator> params = new AsyncTaskClientParams(
							remote_host,
							request_message.subscribeForRotationAngleEventsMethod.port,
							task_id,
							callback);
					subscribers.put(task_id, new ClientAsyncTask<>(params));
					subscribers.get(task_id)
							.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
					interval_sensor_handler.subscribe(callback);
					response = protocol.generate_ok_response();
					Log.d("Status: ", "registered threads: " + subscribers.size());
				}
				break;
				case UnSubscribeForRotationAngleEvents:
				{
					Log.d("Status:", "UnSubscribeForRotationAngleEvents");
					Log.d("Status: ", "registered threads: " + subscribers.size());

					String task_id = request_message.unsubscribeForRotationAngleEventsMethod.task_id;
					AsyncTaskClientParams<BufferBasedSensorDataToBytesTranslator> params = subscribers
							.get(task_id)
							.params();
					if (params == null)
					{
						request_message.status = Message.Status.ERROR;
						request_message.err.code = 5;
						request_message.err.message = "No such a subscription" + task_id + "found to unsubscribe";
						response = protocol.generate_error(request_message);
						return response;
					}

					Log.d("Status", "removing thread #" + task_id);
					params.running = false;
					interval_sensor_handler.unsubscribe(params.callback);
					subscribers.get(task_id).cancel(true);

					if (subscribers.remove(task_id) == null)
					{
						Log.d("Status remove():",
								"The task_id #" + task_id + " is not found! Thus nothing hasn't been deleted.");
						request_message.err.code = 5;
						request_message.err.message = "The task_id #" + task_id + " is not found! Thus nothing hasn't been deleted.";
						response = protocol.generate_error(request_message);
					} else
					{
						response = protocol.generate_ok_response();
						Log.d("Status: ", "registered threads: " + subscribers.size());
					}
				}
				break;
				case ShortVibration:
					vibrateFor(5);
					response = protocol.generate_ok_response();
					break;
				case LongVibration:
					vibrateFor(25);
					response = protocol.generate_ok_response();
					break;
				case DoesNotExist:
					response = protocol.generate_error(request_message);
					break;

				default:
				{
					request_message.err.code = 6;
					request_message.err.message = "Something went wrong.";
					response = protocol.generate_error(request_message);
				}
			}

			return response;
		}

		@SuppressLint("SetTextI18n")
		@Override
		public void onDisconnect(Exception error, String ip)
		{
			runOnUiThread(() ->
			{
				stateTxt.setText("");
				statusTxt.setText("Disconnected");
				String key = "";

				for (Map.Entry<String, ClientAsyncTask<BufferBasedSensorDataToBytesTranslator>> paramsEntry : subscribers
						.entrySet())
				{
					AsyncTaskClientParams<BufferBasedSensorDataToBytesTranslator> params;
					if ((params = paramsEntry.getValue().params()) != null)
					{
						if (params.remote_host_address.equals(ip))
						{
							key = paramsEntry.getKey();
							break;
						}
					}
				}

				ClientAsyncTask<BufferBasedSensorDataToBytesTranslator> clientAsyncTask = subscribers
						.get(key);
				if (clientAsyncTask != null)
				{
					AsyncTaskClientParams<BufferBasedSensorDataToBytesTranslator> params = clientAsyncTask
							.params();
					if (params != null)
					{
						Log.d("Status", "removing thread #" + key);
						params.running = false;
						interval_sensor_handler.unsubscribe(params.callback);
						subscribers.get(key).cancel(true);
						subscribers.remove(key);
					}
				}

			});

		}

		@SuppressLint("SetTextI18n")
		@Override
		public void onResponseSent()
		{
			runOnUiThread(() -> stateTxt.setText("Sent"));
		}

		@SuppressLint("SetTextI18n")
		@Override
		public void onConnect(String ip)
		{
			runOnUiThread(() ->
					statusTxt.setText("Connected: " + ip));
		}
	};

	@SuppressLint("SetTextI18n")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// Register the sensor listeners
		m_vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		interval_sensor_handler = new SensorHandler(this);
		readingYaw = findViewById(R.id.azimuth);
		readingPitch = findViewById(R.id.pitch);
		readingRoll = findViewById(R.id.roll);
		statusTxt = findViewById(R.id.status);
		stateTxt = findViewById(R.id.state);
		startStopSensorBtn = findViewById(R.id.start_stop_button);
		exitBtn = findViewById(R.id.exitButton);
		exitBtn.setOnClickListener(v -> cleanUpAndExit());

		interval_sensor_handler.start();
		if(interval_sensor_handler.isStopped())
			startStopSensorBtn.setText("Start");
		else
			startStopSensorBtn.setText("Stop");

		startStopSensorBtn.setOnClickListener(v ->
		{
			if (interval_sensor_handler.isStopped())
			{
				interval_sensor_handler.start();
				startStopSensorBtn.setText("Stop");
			}
			else
			{
				interval_sensor_handler.stop();
				startStopSensorBtn.setText("Start");
			}
		});

		startStopServerBtn = findViewById(R.id.stopServerBtn);

		startStopServerBtn.setOnClickListener((View v) ->
		{
			if (server.isRunning())
			{
				server.stop();
				for (Map.Entry<String, ClientAsyncTask<BufferBasedSensorDataToBytesTranslator>> paramsEntry : subscribers.entrySet())
				{
					AsyncTaskClientParams<BufferBasedSensorDataToBytesTranslator> params;
					if ((params = paramsEntry.getValue().params()) != null)
					{
						params.running = false;
						interval_sensor_handler.unsubscribe(params.callback);
						subscribers.get(paramsEntry.getKey()).cancel(true);
						subscribers.remove(paramsEntry.getKey());
					}
				}
				startStopServerBtn.setText("Start Server");
			}
			else
			{
				server.start();
				startStopServerBtn.setText("Stop Server");
			}
		});

		/* Values might be empty - uninitialized
		 * But so far we live it like this...
		 */
		new Thread(() ->
		{
			while (true)
			{
				long[] values = interval_sensor_handler.get_XYZ_angle_values();
				runOnUiThread(() ->
				{
					readingRoll.setText(String.valueOf(values[0]) + "°");
					readingPitch.setText(String.valueOf(values[1]) + "°");
					readingYaw.setText(String.valueOf(values[2]) + "°");
				});
			}
		}).start();


		server = new Server(6262, m_callback);
		server.start();

		if (server.isRunning())
			startStopServerBtn.setText("Stop Server");
		else
			startStopServerBtn.setText("Start Server");
	}

	// Vibrate for 500 milliseconds
	void vibrateFor(int milliseconds)
	{
		m_vibrator.vibrate(milliseconds);
	}

	class BufferBasedSensorDataToBytesTranslator extends BufferBasedSensorOrientationUpdateListener implements AsyncClientCallback
	{
		@Override
		public int available(byte[] container)
		{
			int written_bytes = 0;
			long[] data = new long[3];
			if (available(data))
			{
				Log.d("Status:", "New data is available!");
				ByteBuffer message = ByteBuffer.wrap(protocol.generate_wrist_orientation_update_response(
						data));
				written_bytes = message.array().length;
				for (int i = 0; i < written_bytes; ++i)
				{
					container[i] = message.get(i);
				}

				Log.d("Data: ", "" + data[0]);
				Log.d("Reply: ", "" + new String(container, 0, written_bytes));
				return written_bytes;
			}

			return written_bytes;
		}
	}



	void cleanUpAndExit()
	{
		server.stop();
		for (Map.Entry<String, ClientAsyncTask<BufferBasedSensorDataToBytesTranslator>> paramsEntry : subscribers.entrySet())
		{
			AsyncTaskClientParams<BufferBasedSensorDataToBytesTranslator> params;
			if ((params = paramsEntry.getValue().params()) != null)
			{
				params.running = false;
				interval_sensor_handler.unsubscribe(params.callback);
				subscribers.get(paramsEntry.getKey()).cancel(true);
				subscribers.remove(paramsEntry.getKey());
			}
		}
		onDestroy();
		android.os.Process.killProcess(Process.myPid());
	}

}



