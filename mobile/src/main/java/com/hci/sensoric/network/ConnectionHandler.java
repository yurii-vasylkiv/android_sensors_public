package com.hci.sensoric.network;

/**
 * This interface defines methods that handle connection notifications and the data received from the socket connection.
 * The methods are called from the background thread of the AsyncTask.
 * <p>
 * Created by StarWheel on 10/08/13.
 */
public interface ConnectionHandler
{
	public byte[] onReceiveRequest(byte[] data, String remote_host);

	public void onResponseSent();

	public void onDisconnect(Exception error, String ip);

	public void onConnect(String ip);
}
