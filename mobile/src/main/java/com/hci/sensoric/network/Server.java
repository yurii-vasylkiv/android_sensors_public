package com.hci.sensoric.network;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by yurii on 06/11/17.
 */

public class Server
{
	private String m_host;
	private String m_remote_host;
	private int m_port;
	private boolean m_running;
	private ConnectionHandler m_callback;
	private ServerSocket socServer;
	private ArrayList<AsyncConnection> m_connections = new ArrayList<>();

	public Server(int port, ConnectionHandler callback)
	{
		m_callback = callback;
		m_port = port;
		try
		{
			for (Enumeration<NetworkInterface> enumeration = NetworkInterface
					.getNetworkInterfaces(); enumeration.hasMoreElements(); )
			{
				NetworkInterface networkInterface = enumeration.nextElement();
				for (Enumeration<InetAddress> enumerationIpAddr = networkInterface
						.getInetAddresses(); enumerationIpAddr
							 .hasMoreElements(); )
				{
					InetAddress inetAddress = enumerationIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress()
							&& inetAddress.getAddress().length == 4)
					{
						m_host = inetAddress.getHostAddress();
					}
				}
			}
		}
		catch (SocketException e)
		{
			Log.e("ERROR:", e.toString());
		}
	}

	public void start()
	{
		m_running = true;

		new Thread(() ->
		{
			Log.d("SERVER STATUS: ", "Setting up.");

			try
			{
				socServer = new ServerSocket(m_port);
				Socket socClient = null;
				while (m_running)
				{
					Log.d("SERVER STATUS: ", "Listening on: " + m_host + ":" + m_port);
					socClient = socServer.accept();
					m_remote_host = socClient.getInetAddress().getHostAddress();
					m_callback.onConnect(m_remote_host);
					Log.d("SERVER STATUS: ",
							"new client accepted: " + m_remote_host);
					Log.d("SERVER STATUS: ", "Connections: " + m_connections.size());
					m_connections.add(new AsyncConnection());
					m_connections.get(m_connections.size() - 1).
							executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
									new AsyncTaskServerParams(socClient, m_callback));
				}

				socServer.close();
				if (socClient != null)
				{
					socClient.close();
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}).start();

		Log.d("SERVER STATUS: ", "Set up!");
	}

	public void stop()
	{
		m_running = false;
		for (AsyncConnection conn : m_connections)
			conn.interrupt();
		try
		{
			if (socServer != null)
				socServer.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public boolean isRunning()
	{
		return m_running;
	}
}