package com.hci.sensoric.network;

/**
 * Created by yurii on 22/11/17.
 */

public interface AsyncClientCallback
{
	int available(byte[] container);
}
