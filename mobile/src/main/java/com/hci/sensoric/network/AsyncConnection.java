package com.hci.sensoric.network;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.Arrays;

/**
 * AsyncTask which handles the communication with clients
 */
class AsyncConnection extends AsyncTask<AsyncTaskServerParams, Void, Integer>
{
	private byte body_buffer[] = new byte[1024 * 2048];
	private byte header_buffer[] = new byte[11];
	private boolean m_running = true;
	private Socket mySocket;

	protected Integer doInBackground(AsyncTaskServerParams... params)
	{
		Log.d("Call", "doInBackground()");
		byte[] result;
		mySocket = params[0].socket;
		ConnectionHandler callback = params[0].callback;
		try
		{
			InputStream is = mySocket.getInputStream();
			BufferedOutputStream out = new BufferedOutputStream(mySocket.getOutputStream());
			while (mySocket.isConnected())
			{
				int read_bytes = is.read(header_buffer, 0, 11);
				int body_size_to_read = message_header_protocol.extract_body_length(header_buffer);
				if (!message_header_protocol.is_header_ok(header_buffer))
				{
					Log.d("client " + mySocket.getInetAddress().getHostAddress(),
							"Invalid header!");
					result = "Invalid header!".getBytes("ISO-8859-1");
					byte[] reply = message_header_protocol.insert_header(result);
					out.write(reply);
					out.flush();
					mySocket.close();
					return 0;
				}

				//Log.d("Status:", "Reading...");
				read_bytes = is.read(body_buffer, 0, body_size_to_read);

				if (read_bytes != body_size_to_read)
				{
					Log.d("networking: ", "Invalid number of bytes read from socket!");
					result = "Invalid number of bytes read from socket!".getBytes("ISO-8859-1");
					callback.onDisconnect(null, mySocket.getInetAddress().getHostAddress());
					mySocket.close();
					Log.d("Status:", "Socket closed!");
					return 0;
				}

				String body_message = new String(body_buffer, 0, body_size_to_read);
				if (body_message.equals("test"))
				{
					result = "OK".getBytes("ISO-8859-1");
					//Log.d("Status:", "Writing...");
					out.write(message_header_protocol.insert_header(result));
					out.flush();
					continue;
				}

				//Log.d("client " + mySocket.getInetAddress().getHostAddress() + " asked: ", body_message);
				result = callback.onReceiveRequest(Arrays.copyOfRange(body_buffer,
						0,
						body_size_to_read), mySocket.getInetAddress().getHostAddress());
				if (result == null)
						result = "ERROR".getBytes();

				byte[] reply = message_header_protocol.insert_header(result);
				//Log.d("server:", new String(result, "ISO-8859-1"));
				//Log.d("Status:", "Writing...");
				out.write(reply);
				out.flush();
				callback.onResponseSent();
			}
		}
		catch (IOException e)
		{
			try
			{
				mySocket.close();
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

		return 0;
	}

	protected void onPostExecute(String s)
	{
		Log.d("call", "onPostExecute()");
	}

	void interrupt()
	{
		if (mySocket == null)
			return;

		try
		{
			mySocket.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
