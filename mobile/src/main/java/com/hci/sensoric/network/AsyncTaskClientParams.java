package com.hci.sensoric.network;

/**
 * Created by yurii on 21/11/17.
 */

public class AsyncTaskClientParams<T>
{
	public String remote_host_address;
	public short port;
	public boolean running;
	public String task_id;
	public T callback;


	public AsyncTaskClientParams(String remote_host_address, short port, String task_id, T callback)
	{
		this.task_id = task_id;
		this.callback = callback;
		this.remote_host_address = remote_host_address;
		this.port = port;
		running = true;
	}
}
